import random
from django import template
register = template.Library()

@register.filter(name='rand')
def rand(l):
    return(l[random.randint(0,len(l)-1)])

@register.assignment_tag
def randQuote(qL):
    q = random.randint(0,len(qL)-1)
    randQ = qL[q]
    qL.pop(q)
    return randQ;
