var HttpClient = function() {
    this.get = function(aUrl, aCallback) {
        var anHttpRequest = new XMLHttpRequest();
        anHttpRequest.onreadystatechange = function() {
            if (anHttpRequest.readyState == 4 && anHttpRequest.status == 200)
                aCallback(anHttpRequest.responseText);
        }

        anHttpRequest.open( "GET", aUrl, true );
        anHttpRequest.send( null );
    }
}

tweet = new HttpClient();
tweet.get('https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=thomas_1151&count=1', function(response) {
    console.log(response)
});
