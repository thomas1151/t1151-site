

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');
function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});



// Submit post on submit
$('#chat-form').on('submit', function(event){
    event.preventDefault();
    console.log("form submitted!")  // sanity check
    post();
});
function post() {
    console.log("create post is working!") // sanity check
    $.ajax({
        url : "post/", // the endpoint
        type : "POST", // http method
        data : { action : $('#chat-form-text').val(),
                 sender   : $('#chat-form-user').val() }, // data sent with the post request

        // handle a successful response

        success : function(json) {
            $('#chat-form-text').val(''); // remove the value from the input
            console.log(json); // log the returned json to the console
            $(".chat").append('<div class="action"><div class="pure-g"><div class="pure-u-4-24 sender ioo'+json.sender_intorext+'">'+json.sender+'</div><div class="pure-u-4-24 time"><p>'+json.senddate+'</p></div><div class="pure-u-16-24 text">'+json.text+'</div></div></div>')
            $(".action-wrap").scrollTop($(".chat")[0].scrollHeight);




                                   
                                
            console.log("success"); // another sanity check
        },

        // handle a non-successful response
        error : function(xhr,errmsg,err) {
            $('#results').html("<div class='alert-box alert radius' data-alert>Oops! We have encountered an error: "+errmsg+
                " <a href='#' class='close'>&times;</a></div>"); // add the error to the dom
            console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
        }
    });
};