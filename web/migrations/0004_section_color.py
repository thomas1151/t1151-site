# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0003_auto_20160328_2140'),
    ]

    operations = [
        migrations.AddField(
            model_name='section',
            name='color',
            field=models.CharField(default='ffffff', max_length=6),
        ),
    ]
