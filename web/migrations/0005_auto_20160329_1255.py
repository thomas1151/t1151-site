# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0004_section_color'),
    ]

    operations = [
        migrations.AddField(
            model_name='section',
            name='position',
            field=models.IntegerField(default=-1),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='section',
            name='color',
            field=models.CharField(max_length=6, default='0'),
        ),
    ]
