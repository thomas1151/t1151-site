# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Invoice',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('description', models.CharField(max_length=200)),
                ('date', models.DateTimeField(verbose_name='Date Created')),
                ('invType', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='InvoiceAction',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('date', models.DateTimeField()),
                ('work', models.CharField(max_length=100)),
                ('quantity', models.IntegerField()),
                ('price', models.IntegerField()),
                ('invoice', models.ForeignKey(to='web.Invoice')),
            ],
        ),
        migrations.CreateModel(
            name='Location',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('line1', models.CharField(max_length=40)),
                ('line2', models.CharField(max_length=40)),
                ('line3', models.CharField(max_length=40)),
                ('postcode', models.CharField(max_length=8)),
                ('lat', models.CharField(max_length=4)),
                ('long', models.CharField(max_length=4)),
            ],
        ),
        migrations.CreateModel(
            name='Organisation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('name', models.CharField(max_length=200)),
                ('location', models.ForeignKey(to='web.Location')),
            ],
        ),
        migrations.CreateModel(
            name='Peer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('ip', models.CharField(max_length=50)),
                ('nick', models.CharField(max_length=12)),
                ('intorext', models.IntegerField(default=0)),
                ('credate', models.DateTimeField(verbose_name='Date Created')),
                ('age', models.IntegerField()),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Person',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('fname', models.CharField(max_length=20)),
                ('lname', models.CharField(max_length=20)),
                ('address', models.ForeignKey(to='web.Location')),
                ('organisation', models.ForeignKey(to='web.Organisation')),
            ],
        ),
        migrations.CreateModel(
            name='Section',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('name', models.CharField(max_length=20)),
                ('pl', models.IntegerField()),
            ],
        ),
        migrations.AddField(
            model_name='invoice',
            name='eRef',
            field=models.ForeignKey(to='web.Person'),
        ),
        migrations.AddField(
            model_name='invoice',
            name='iRef',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='invoice',
            name='invoiceAddr',
            field=models.ForeignKey(to='web.Location', related_name='invAddr'),
        ),
        migrations.AddField(
            model_name='invoice',
            name='jobAddr',
            field=models.ForeignKey(to='web.Location', related_name='jobAddr'),
        ),
        migrations.AddField(
            model_name='invoice',
            name='organisation',
            field=models.ForeignKey(to='web.Organisation'),
        ),
    ]
