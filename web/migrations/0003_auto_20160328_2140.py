# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0002_auto_20160325_2254'),
    ]

    operations = [
        migrations.CreateModel(
            name='Global',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=30)),
                ('summary', models.CharField(max_length=120)),
            ],
        ),
        migrations.CreateModel(
            name='Item',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=30)),
                ('info', models.CharField(max_length=500)),
                ('picID', models.IntegerField()),
                ('iconID', models.CharField(max_length=10, default='0')),
            ],
        ),
        migrations.RemoveField(
            model_name='invoice',
            name='eRef',
        ),
        migrations.RemoveField(
            model_name='invoice',
            name='iRef',
        ),
        migrations.RemoveField(
            model_name='invoice',
            name='invoiceAddr',
        ),
        migrations.RemoveField(
            model_name='invoice',
            name='jobAddr',
        ),
        migrations.RemoveField(
            model_name='invoice',
            name='organisation',
        ),
        migrations.RemoveField(
            model_name='invoiceaction',
            name='invoice',
        ),
        migrations.RemoveField(
            model_name='organisation',
            name='location',
        ),
        migrations.RemoveField(
            model_name='person',
            name='address',
        ),
        migrations.RemoveField(
            model_name='person',
            name='organisation',
        ),
        migrations.RemoveField(
            model_name='section',
            name='pl',
        ),
        migrations.AddField(
            model_name='section',
            name='description',
            field=models.CharField(max_length=500, default=0),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='section',
            name='name',
            field=models.CharField(max_length=30),
        ),
        migrations.DeleteModel(
            name='Invoice',
        ),
        migrations.DeleteModel(
            name='InvoiceAction',
        ),
        migrations.DeleteModel(
            name='Location',
        ),
        migrations.DeleteModel(
            name='Organisation',
        ),
        migrations.DeleteModel(
            name='Person',
        ),
        migrations.AddField(
            model_name='item',
            name='section',
            field=models.ForeignKey(to='web.Section'),
        ),
    ]
