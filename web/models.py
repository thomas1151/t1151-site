from django.db import models
from django.contrib.auth.models import User

# Create your models here.


class Peer(models.Model):
    user = models.OneToOneField(User)
    ip = models.CharField(max_length=50)
    nick = models.CharField(max_length=12)
    intorext = models.IntegerField(default=0)
    credate = models.DateTimeField("Date Created")
    age = models.IntegerField()
    def __str__(self):
        return self.nick

class Global(models.Model):
    name = models.CharField(max_length=30)
    summary = models.CharField(max_length=120)
class Section(models.Model):
    name = models.CharField(max_length=30)
    description = models.CharField(max_length=500)
    color = models.CharField(max_length=6,default='0')
    position = models.IntegerField()
    def __str__(self):
        return self.name
class Item(models.Model):
    name = models.CharField(max_length=30)
    info = models.CharField(max_length=500)
    picID = models.IntegerField()
    iconID = models.CharField(max_length=10,default='0')
    section = models.ForeignKey(Section)
    def __str__(self):
        return self.name
