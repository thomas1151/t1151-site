from django.shortcuts import get_object_or_404,render
from django.http import HttpResponseRedirect, HttpResponse,Http404,JsonResponse
from django.template.response import TemplateResponse
from django.core.urlresolvers import reverse
from django.views import generic
from django.views.generic import View
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.core.exceptions import ObjectDoesNotExist
from .models import Global,Section,Item



import datetime
import random
import string
colors = [
"29ABE2",
"0A54A3",
#"232930"
]
#Using this type for index
class IndexView(View):
    def get(self,request):
        globalData = Global.objects.get(pk=1)
        sectionalData = []
        for s in Section.objects.filter():
            sectionalData.append(s)
            sectionalData[-1] = {}
            sectionalData[-1]['name'] = s.name
            sectionalData[-1]['description'] = s.description
            sectionalData[-1]['id'] = s.id
            sectionalData[-1]['position'] = s.position
            sectionalData[-1]['color'] = s.color
            sectionalData[-1]['objects'] = []
            for i in Item.objects.filter(section_id=s.id):
                sectionalData[-1]['objects'].append(i)
        sectionalData =  sorted(sectionalData, key=lambda k: k['position'])
        testimonialData = []
        for t in Item.objects.filter(section_id=5):
            testimonialData.append(t)

        return TemplateResponse(request,'web/index.html',{
                                                     'global': globalData,
                                                     'sectionalData':sectionalData,
                                                     'testimonialData':testimonialData,
                                                     'colors':colors,
                                                        })
    @csrf_protect
    def post(request):
        if request.method == 'POST':
             if request.POST['action'] == 'newInvoice':
                response_data = {}
                iaFormSet = LinkFormSet(request.POST)

                #
                # try:
                #     with transaction.atomic():
                #         #Replace the old with the new
                #         UserLink.objects.filter(user=user).delete()
                #         UserLink.objects.bulk_create(new_links)
                #
                #         # And notify our users that it worked
                #         messages.success(request, 'You have updated your profile.')
                #
                # except IntegrityError: #If the transaction failed
                #     messages.error(request, 'There was an error saving your profile.')
                #     return redirect(reverse('profile-settings'))


#                invoiceAction = InvoiceAction(
#                                            date = request.Post.get()
#                                            work
#                                            quantity
#                                            price
#                                            invoice
#                )

             return JsonResponse(response_data)

        else:

            return JsonResponse({"nothing to see": "this isn't happening"})



#Maybe provide info on specific action, not sure

# class testimonialView(View):
#     de


#Shows all data availble about USer, both this and above
#will help with learning about patterns etc.
class UserView(generic.DetailView):
    template_name = 'web/results.html'
