from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static

from . import views

app_name = 'web'
urlpatterns = [
    url(r'^$',  views.IndexView.as_view(), name='index'),
    url(r'^post/$',  views.IndexView.post),
    url(r'^login/$', 'django.contrib.auth.views.login'),
    url(r'^logout/$', 'django.contrib.auth.views.logout'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
